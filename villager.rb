LIMIT = 100_000_000_000_000
class QTable
  def initialize
    @table = {}
  end

  def blank_values
    {
      get_mango: rand,
      get_tree: rand,
      finish_day: rand
    }
  end

  def [](state, action = nil)
    by_state = (@table[state] ||= blank_values)
    action ? by_state[action] : by_state
  end

  def []=(state, action, value)
    (@table[state] ||= blank_values)[action] = value
  end
end

class Villager
  attr_accessor :day_q_table, :stamina_func, :stamina,
                :mango_hapiness_func, :tree_hapiness_func,
                :mangos_on_tree_func, :trees_func,
                :mango_cost, :tree_cost, :day_finished,
                :total_trees, :mangos_on_tree, :days, :trees_with_mango,
                :got_mangos, :got_trees, :epsilon, :alpha, :gamma
  def initialize(**attrs)
    @epsilon = 0.01
    @alpha = rand.round(2)
    @gamma = rand.round(2)
    attrs.each { |k, v| instance_variable_set(:"@#{k}", v) }
    @day_q_table = QTable.new
    # @trade_q_table = QTable.new
  end

  def state
    "#{got_mangos} #{got_trees} #{stamina} #{mangos_on_tree} #{total_trees}"
  end

  def step(action)
    start_state = state
    reward = case action
             when :get_mango
               if stamina >= mango_cost && trees_with_mango > 0
                 hapiness_was = mango_hapiness_func.call(got_mangos)
                 self.got_mangos += mangos_on_tree
                 self.trees_with_mango -= 1
                 self.stamina -= mango_cost
                 mango_hapiness_func.call(got_mangos) - hapiness_was
               else
                 -1
               end
             when :get_tree
               if stamina >= tree_cost && total_trees > got_trees
                 hapiness_was = tree_hapiness_func.call(got_trees)
                 self.got_trees += 1
                 self.trees_with_mango -= 1 if total_trees - trees_with_mango <= 0
                 self.stamina -= tree_cost
                 tree_hapiness_func.call(got_trees) - hapiness_was
               else
                 -1
               end
             when :finish_day
               self.day_finished = true
               punishment = 0
               punishment -= 5 if stamina >= mango_cost && trees_with_mango > 0
               punishment -= 5 if stamina >= tree_cost && total_trees > got_trees

               punishment
             end
    start_value = day_q_table[start_state, action]
    max_value = day_q_table[state].values.max # by new state
    new_value = (((1 - alpha) * start_value) + (alpha * (reward + (gamma * max_value)))).to_f
    new_value = 0 if new_value.nan?
    new_value = LIMIT if new_value > LIMIT
    new_value = -LIMIT if new_value < -LIMIT
    day_q_table[start_state, action] = new_value
    start_day! if day_finished
  end

  def take_action
    if rand < epsilon
      %i[get_mango get_tree get_mango get_tree get_mango get_tree finish_day].sample
    else
      day_q_table[state].max_by { |_k, v| v }[0]
    end
  end

  def start_day!
    puts "\e[H\e[2 "

    @hapiness_stats ||= []
    @trees_stats ||= []
    @mangos_stats ||= []
    @stamina_stats ||= []

    @hapiness_stats << (mango_hapiness_func.call(got_mangos) + tree_hapiness_func.call(got_trees))
    @trees_stats << got_trees
    @mangos_stats << got_mangos
    @stamina_stats << stamina

    if !@day || @day > 10_000
      @epsilon -= 0.0001 if @epsilon > 0.0001
      @start ||= Time.now
      @day = 0
      puts
      puts "speed: #{(10_000 / (Time.now - @start)).ceil} iterations/s        "
      puts "hapiness: \t#{@hapiness_stats.sum / @hapiness_stats.count} #{@hapiness_stats.max} "\
           "#{@hapiness_stats.min}     "
      puts "trees: \t\t#{@trees_stats.sum / @trees_stats.count} #{@trees_stats.max} #{@trees_stats.min}     "
      puts "mangos: \t#{@mangos_stats.sum / @mangos_stats.count} #{@mangos_stats.max} #{@mangos_stats.min}    "
      puts "left_stamina: \t#{@stamina_stats.sum / @stamina_stats.count} #{@stamina_stats.max} "\
           "#{@stamina_stats.min}     "
      @start = Time.now
      @hapiness_stats = []
      @trees_stats = []
      @mangos_stats = []
      @stamina_stats = []
    end
    @day += 1
    self.mangos_on_tree = mangos_on_tree_func.call(got_mangos)
    self.total_trees = trees_func.call(got_trees, got_mangos)
    self.trees_with_mango = total_trees
    self.got_trees = 0
    self.got_mangos = 0
    self.stamina = stamina_func.call
    self.days += 1
    self.day_finished = false
  end
end
