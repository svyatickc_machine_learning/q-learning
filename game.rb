class Game
  attr_reader :players

  def initialize(players)
    @players = players
  end

  def play_day!
    players.each(&:play_day!)
  end

  def players_stats
    players.map(&:stats)
  end

  def trade!(times = 3)
    players.each do |trador1|
      players.each do |trador2|
        next if trador1 == trador2

        times.times { trador1.trade_with!(trador2) }
      end
    end
  end

  def finish_day!
    players.each(&:finish_day!)
  end
end
