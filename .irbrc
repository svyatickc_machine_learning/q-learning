require './villager'
$stdout.sync = true
def a
  pushb Villager.new(
    stamina_func: -> { 50 },
    mango_hapiness_func: ->(count) { count },
    tree_hapiness_func: ->(count) { (count * 3.6).ceil },
    mangos_on_tree_func: ->(count) { (110 - count) / 50 },
    trees_func: ->(_trees, _mangos) { 17 },
    mango_cost: 1, tree_cost: 3, total_trees: 1, trees_with_mango: 0,
    mangos_on_tree: 1, got_mangos: 1, got_trees: 1, stamina: 1, days: 0
  )
end
